===========================================
Run jwilder/nginx-proxy with docker-compose
===========================================

Copyright (c) 2015 Thomas Kock, License: MIT (see LICENSE.MIT.txt)

Uses https://github.com/jwilder/nginx-proxy

Filesystem layout
-----------------

::

  volumes
    +--etc
       +--nginx
          +--certs     -> mounted to /etc/nginx/certs
  docker-compose.yml


Configuration
-------------

Copy SSL certificate files into ./volumes/etc/nginx/certs


Startup
-------

Initial startup with ``docker-compose up``, later with ``docker-compose start``.

Reconfigurations or modifications::

  docker-compose stop  # or CTRL-C
  docker-compose rm
  docker-compose up

